# SimpleReactApp

Simple server side ReactJS application

### Install & Setup
1. Download and install [Docker.io](https://docker.io): ``docker docker-cli docker-compose``
2. Start in root directory run command: ``docker-compose up -d --build``
3. Enter browser ``http://localhost:3000``
4. Stop in root directory run command: ``docker-compose down``

### Development
#### Server (based webpack-stream)
1. Work directory ``api`` in root directory
2. Port ``3080``
#### Client
1. Work directory ``my-app`` in root directory
2. ReactJS source in ``my-app/src``
3. Port ``3000``
