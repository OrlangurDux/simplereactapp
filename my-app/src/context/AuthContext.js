import {createContext} from 'react'

function nothing() {}

export const AuthContext = createContext({
  token: null,
  login: nothing,
  logout: nothing,
  isAuthenticated: false
})
