import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom'
import {useRoutes} from './routes'
import {Header} from './components/Header'
import {Footer} from './components/Footer'
import {AuthContext} from "./context/AuthContext";
import {useAuth} from "./hooks/auth.hook";
import {Loader} from "./components/Loader";

export const App = () => {

    const {token, login, logout, ready} = useAuth()
    const isAuthenticated = !!token
    const routes = useRoutes(isAuthenticated)

    if (!ready) {
        return <Loader />
    }

    const onClickAlert = (e) => {
        alert('Yes');
    }

    return (
        <AuthContext.Provider value={{
            token, login, logout, isAuthenticated
        }}>
            <div className="App">
                <Router>
                    <Header isAuthenticated = {isAuthenticated}/>
                    <div className="container mt-4">
                        {routes}
                    </div>
                    <Footer
                        onClickAlert={onClickAlert}/>
                </Router>
            </div>
        </AuthContext.Provider>
    )
}

export default App;
