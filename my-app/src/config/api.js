
const ApiRoute = {
    login: '/api/v1/login',
    register: '/api/v1/register',
    users: {
        list: '/api/v1/users',
        show: id => `/api/v1/users/${id}`,
    },
    products: {
        list: '/api/v1/products',
        show: id => `/api/v1/products/${id}`,
    }
}

export default ApiRoute