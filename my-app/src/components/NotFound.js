import React from 'react'

export const NotFound = () => (
    <>
        <div className="alert alert-info">Не найдено</div>
    </>

)
