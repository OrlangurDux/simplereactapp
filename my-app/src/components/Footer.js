import React from 'react'

export const Footer = ({onClickAlert}) => {
    return (
        <>
            <div className="mt-4 footer">
                <span onClick={(e) => onClickAlert()}>Подвал (click)</span>
            </div>
        </>
    )
}
