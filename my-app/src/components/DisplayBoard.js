import React from 'react'
import {Loader} from "./Loader";

export const DisplayBoard = ({numberOfUsers, getUsersList, loading}) => {
    return(
        <div className="display-board">
            <h4>Создано пользователей</h4>
            <div className="number">
            {loading ? <Loader /> : numberOfUsers}
            </div>
            <div className="btn">
                <button type="button" onClick={getUsersList} className="btn btn-warning">Обновить список пользователей</button>
            </div>
        </div>
    )
}