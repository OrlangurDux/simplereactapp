import React, {useEffect, useCallback, useState, useContext} from 'react'
import {Link, useParams} from 'react-router-dom'
import {Card} from 'react-bootstrap'
import ApiRoute from "../../config/api";
import {useHttp} from "../../hooks/http.hook";
import {AuthContext} from "../../context/AuthContext";
import {NotFound} from "../NotFound";
import {Loader} from "../Loader";

export const ProductShow = () => {

    const {id} = useParams()
    const {token} = useContext(AuthContext)
    const {loading, request} = useHttp()
    const [product, setProduct] = useState({})
    const [exists, setExists] = useState(true)

    const getProduct = useCallback(async () => {
        try {
            const fetched = await request(ApiRoute.products.show(id), 'GET', null, {
                'Authorization': `${token}`
            })
            setProduct(fetched.data)
        } catch (e) {
            setExists(false)
        }
    }, [request, token, id])

    useEffect(() => {
        getProduct()
    }, [getProduct])

    if (loading)
        return <Loader/>

    if (!exists)
        return <NotFound/>

    return (
        <>
            {!loading && product.id && <Card>
                <Card.Header>ID <strong>{product.id}</strong></Card.Header>
                <Card.Body>
                    <Card.Title><strong>{product.name}</strong></Card.Title>
                    <Card.Text>
                        {product.detail}

                        <div className="text-muted mt-4">
                            <div className="">Создан: <strong>{product.created_at}</strong></div>
                            <div className="">Изменен: <strong>{product.updated_at}</strong></div>
                        </div>

                    </Card.Text>
                    <Link to={`/products/${product.id}/edit`} className="btn btn-primary">Редактировать</Link>
                    <Link to="/products" className="btn btn-danger ml-2">Вернуться к списку</Link>
                </Card.Body>
            </Card>}
        </>
    )
}
