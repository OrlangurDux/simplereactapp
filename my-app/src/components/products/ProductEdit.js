import React, {useState, useCallback, useContext, useEffect} from 'react'
import useSimpleReactValidator from "../../hooks/validate.hook";
import ApiRoute from "../../config/api";
import {useHttp} from "../../hooks/http.hook";
import {Button, Spinner} from 'react-bootstrap'
import {AuthContext} from "../../context/AuthContext";
import {useHistory, useParams, Link} from 'react-router-dom'
import {NotFound} from "../NotFound";

export const ProductEdit = () => {

    const validateRules = {};
    const {token} = useContext(AuthContext)
    const {loading, request, error} = useHttp()
    const history = useHistory()
    const {id} = useParams()
    const [exists, setExists] = useState(true)

    const [validator] = useSimpleReactValidator({...validateRules, autoForceUpdate: true});
    const [product, setProduct] = useState({
        id: id,
        name: '',
        detail: ''
    })

    const getProduct = useCallback(async () => {
        try {
            const fetched = await request(ApiRoute.products.show(id), 'GET', null, {
                'Authorization': `${token}`
            })
            setProduct(fetched.data)
        } catch (e) {
            setExists(false)
        }
    }, [request, token, id])

    useEffect(() => {
        getProduct()
    }, [getProduct])

    const changeHandler = event => {
        setProduct({ ...product, [event.target.name]: event.target.value })
    }

    const editProduct = useCallback(async () => {
        try {
            await request(ApiRoute.products.show(id), 'PATCH', product, {
                'Authorization': `${token}`
            })
            history.push('/products')

        } catch (e) {}


    }, [request, product, token, history, id])



    const editProductHandler = (e) => {
        if (validator.allValid()) {
            editProduct()
        } else {
            validator.showMessages()
        }
    }

    if (!exists)
        return <NotFound/>

    return (
        <>
            {product.name &&
            <div className="container">
                <div className="row">
                    <div className="col-md-7 mrgnbtm">
                        <h2>Редактирование</h2>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Название</label>
                                <input type="text" onChange={changeHandler} className="form-control"
                                       name="name" id="name"
                                       placeholder="Название" value={product['name']}
                                       onBlur={() => validator.showMessageFor('name')}/>
                                {validator.message('Название', product['name'], 'required|string')}
                            </div>
                            <div className="form-group">
                                <label htmlFor="detail">Описание</label>
                                <input type="text" onChange={changeHandler} className="form-control"
                                       name="detail" id="detail" placeholder="Описание"
                                       value={product['detail']}
                                       onBlur={() => validator.showMessageFor('detail')}/>
                                {validator.message('Описание', product['detail'], 'required|string')}
                            </div>
                            <Button variant="danger" onClick={editProductHandler} disabled={loading}>
                                Отправить
                                {loading && <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" />}
                            </Button>
                            <Link to="/products" className="btn btn-outline-secondary ml-2">
                                Отмена
                            </Link>
                            {error && <div className="alert alert-danger mt-2 small" role="alert">{error}</div>}
                        </form>
                    </div>
                </div>
            </div>
            }
        </>
    )
}
