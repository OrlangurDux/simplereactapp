import React, {useState, useCallback, useContext} from 'react'
import useSimpleReactValidator from "../../hooks/validate.hook";
import ApiRoute from "../../config/api";
import {useHttp} from "../../hooks/http.hook";
import {Button, Spinner} from 'react-bootstrap'
import {AuthContext} from "../../context/AuthContext";
import {Link, useHistory} from 'react-router-dom'

export const ProductCreate = () => {

    const validateRules = {};
    const {token} = useContext(AuthContext)
    const {loading, request, error} = useHttp()
    const history = useHistory()

    const [validator] = useSimpleReactValidator({...validateRules, autoForceUpdate: true});
    const [product, setProduct] = useState({
        name: '',
        detail: ''
    })


    const changeHandler = event => {
        setProduct({ ...product, [event.target.name]: event.target.value })
    }

    const createProduct = useCallback(async () => {
        try {
            await request(ApiRoute.products.list, 'POST', product, {
                'Authorization': `${token}`
            })
            history.push('/products')
        } catch (e) {}

    }, [request, product, token, history])



    const createProductHandler = (e) => {
        if (validator.allValid()) {
            createProduct()
        } else {
            validator.showMessages()
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-7 mrgnbtm">
                    <h2>Добавить продукцию</h2>
                    <form>
                        <div className="form-group">
                            <label htmlFor="name">Название</label>
                            <input type="text" onChange={changeHandler} className="form-control"
                                   name="name" id="name"
                                   placeholder="Название" value={product['name']}
                                   onBlur={() => validator.showMessageFor('name')}/>
                            {validator.message('Название', product['name'], 'required|string')}
                        </div>
                        <div className="form-group">
                            <label htmlFor="detail">Описание</label>
                            <input type="text" onChange={changeHandler} className="form-control"
                                   name="detail" id="detail" placeholder="Описание"
                                   value={product['detail']}
                                   onBlur={() => validator.showMessageFor('detail')}/>
                            {validator.message('Описание', product['detail'], 'required|string')}
                        </div>
                        <Button variant="danger" onClick={createProductHandler} disabled={loading}>
                            Создать
                            {loading && <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" />}
                        </Button>
                        <Link to="/products" className="btn btn-outline-secondary ml-2">
                            Отмена
                        </Link>
                        {error && <div className="alert alert-danger mt-2 small" role="alert">{error}</div>}
                    </form>
                </div>
            </div>
        </div>
    )
}
