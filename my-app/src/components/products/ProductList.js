import React, {useContext} from 'react'
import {Link, useRouteMatch} from 'react-router-dom'
import {Table, Badge} from 'react-bootstrap'
import ApiRoute from "../../config/api";
import {AuthContext} from "../../context/AuthContext";
import {useHttp} from "../../hooks/http.hook";


export const ProductList = ({products, getProductsList}) => {

    const {token} = useContext(AuthContext)
    const {request} = useHttp()
    const {path} = useRouteMatch()

    const deleteProduct = async (id) => {
        try {
            await request(ApiRoute.products.show(id), 'DELETE',null, {
                'Authorization': `${token}`
            })
            getProductsList()

        } catch (e) {}

    }

    if (!products.length) {
        return <div className="alert alert-info">Продукции пока нет</div>
    }

    const ProductRow = (product) => {

        return (
            <tr key={product.id}>
                <td>{product.id}</td>
                <td><Link to={`${path}/${product.id}`}>{product.name}</Link></td>
                <td>{product.detail}</td>
                <td><Link to={`${path}/${product.id}/edit`}><Badge variant="primary">Редактировать</Badge></Link> <Link to="#" onClick={() => deleteProduct(product.id)}><Badge variant="danger">Удалить</Badge></Link></td>
            </tr>
        )
    }

    const productsTable = products.map((product) => ProductRow(product))

    return(

            <Table striped bordered hover size="sm">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                {productsTable}
                </tbody>
            </Table>
    )
}