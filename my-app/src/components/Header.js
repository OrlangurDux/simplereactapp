import React, {useContext} from 'react'
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from '../context/AuthContext'
import {Button} from 'react-bootstrap'

export const Header = ({isAuthenticated}) => {

    const auth = useContext(AuthContext)
    const history = useHistory()

    const logoutHandler = event => {
        event.preventDefault()
        auth.logout()
        history.push('/')
    }

    /*
    *
    * */

    return(
            <div className="header">
                <h1>React With NodeJS</h1>
                <div className="menu">
                    <NavLink exact to="/" activeClassName="selected">Главная</NavLink>
                    <NavLink exact to="/users" activeClassName="selected">Пользователи</NavLink>
                    {isAuthenticated && <NavLink to="/products" activeClassName="selected">Продукция</NavLink>}
                    <NavLink exact to="/info" activeClassName="selected">Информация</NavLink>
                    <NavLink exact to="/graph" activeClassName="selected">График</NavLink>
                    {isAuthenticated && <Button className="ml-4" onClick={logoutHandler}>Выйти</Button>}
                    {!isAuthenticated && <NavLink exact to="/login" activeClassName="active" className="btn btn-success ml-4">Авторизация</NavLink>}
                </div>
            </div>
    )
}