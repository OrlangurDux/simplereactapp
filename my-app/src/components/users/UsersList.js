import React from 'react'
import {Table} from 'react-bootstrap'

export const UsersList = ({users}) => {

    if (!users.length) {
        return <div className="alert alert-info">Пользователей пока нет</div>
    }

    const UserRow = (user) => {

        return(
              <tr key = {user.id}>
                  <td>{user.id}</td>
                  <td>{user.name}</td>
                  <td>{user.last_name}</td>
                  <td>{user.email}</td>
              </tr>
          )
    }

    const userTable = users.map((user) => UserRow(user))

    return(
        <div className="container">
            <h2>Пользователи</h2>
            <Table striped bordered hover size="sm">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                {userTable}
                </tbody>
            </Table>
        </div>
    )
}
