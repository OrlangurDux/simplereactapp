import React, {useState, useCallback} from 'react'
import useSimpleReactValidator from "../../hooks/validate.hook";
import ApiRoute from "../../config/api";
import {useHttp} from "../../hooks/http.hook";
import {Button, Spinner} from 'react-bootstrap'

export const CreateUser = ({getUsersList}) => {

    const validateRules = {
        messages: {
            email: 'Введите корректный email',
        },
        validators: {
            password: {
                message: ':attribute не совпадают.',
                rule: function (val, params, validator) {
                    //console.dir(params);
                    return val === params[0];
                }
            }
        }
    };

    const [validator] = useSimpleReactValidator({...validateRules, autoForceUpdate: true});
    const [user, setUser] = useState({
        name: '',
        last_name: '',
        email: '',
        password: '',
        c_password: '',
    })
    const {loading, request, error, clearError} = useHttp()
    const ajaxError = 'Произошла ошибка, из-за которой форма не была отправлена.'

    const clearForm = useCallback( () => {
        setUser({
            name: '',
            last_name: '',
            email: '',
            password: '',
            c_password: '',
        })
        clearError()
    }, [clearError])

    const changeHandler = event => {
        setUser({ ...user, [event.target.name]: event.target.value })
    }

    const createUser = useCallback(async () => {
        try {
            await request(ApiRoute.register, 'POST', user)
            clearForm()
            getUsersList()
        } catch (e) {}

    }, [request, user, clearForm, getUsersList])



    const createUserHandler = (e) => {
        //console.log(validator.check(user['firstname'], 'required|alpha'))
        if (validator.allValid()) {
            createUser()
        } else {
            validator.showMessages()
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-7 mrgnbtm">
                    <h2>Создать пользователя</h2>
                    <form>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <label htmlFor="name">Имя</label>
                                <input type="text" onChange={changeHandler} className="form-control"
                                       name="name" id="name"
                                       placeholder="Имя" value={user['name']}
                                       onBlur={() => validator.showMessageFor('name')}/>
                                {validator.message('Имя', user['name'], 'required|string')}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="last_name">Фамилия</label>
                                <input type="text" onChange={changeHandler} className="form-control"
                                       name="last_name" id="last_name" placeholder="Фамилия"
                                       value={user['last_name']}
                                       onBlur={() => validator.showMessageFor('last_name')}/>
                                {validator.message('Фамилия', user['last_name'], 'required|string')}
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-12">
                                <label htmlFor="email">Email</label>
                                <input type="email" onChange={changeHandler}
                                       className="form-control"
                                       name="email" id="email" aria-describedby="passwordHelp" placeholder="Email"
                                       value={user['email']}
                                       onBlur={() => validator.showMessageFor('email')}/>
                                {validator.message('Email', user['email'], 'required|email')}
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-md-6">
                                <label htmlFor="password">Пароль</label>
                                <input type="password" onChange={changeHandler}
                                       className="form-control"
                                       name="password" id="password" aria-describedby="passwordHelp" placeholder=""
                                       value={user['password']}
                                       onBlur={() => validator.showMessageFor('password')}/>
                                {validator.message('Пароль', user['password'], 'required')}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="c_password">Подтверждение</label>
                                <input type="password" onChange={changeHandler}
                                       className="form-control"
                                       name="c_password" id="c_password" placeholder=""
                                       value={user['c_password']}
                                       onBlur={() => validator.showMessageFor('c_password')}/>
                                {validator.message('Подтверждение', user['c_password'], 'required|password:' + user['password'])}
                            </div>
                        </div>
                        {validator.messageWhenPresent(ajaxError, {
                            element: message => <div className="alert alert-warning" role="alert">{message}</div>
                        })}
                        <Button variant="danger" onClick={createUserHandler} disabled={loading}>
                            Создать
                            {loading && <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" />}
                        </Button>
                        {error && <div className="alert alert-danger mt-2 small" role="alert">{error}</div>}
                    </form>
                </div>
            </div>
        </div>
    )
}
