import React from 'react'
import ReactDOM from 'react-dom'

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()}
    }

    componentDidMount() {
        this.timerID = setInterval(
            ()=>this.tick(),
            1000
        )
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick(){
        this.setState({
            date: new Date()
        })
    }

    render(){
        return (
            <div>
                <h1>Class Clock</h1>
                <h2>Current time {this.state.date.toLocaleTimeString()}</h2>
            </div>
        )
    }
}

class Toogle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isToogleOn: true,
                      label: props.label};
        //this.state = {label: props.label}
        //this.handleClick = this.handleClick.bind(this);
        console.log(this.state);
    }

    handleClick = ()=>{
        this.setState(state => ({
            isToogleOn: !state.isToogleOn
        }));
    }

    render(){
        return (
            <button onClick={this.handleClick}>
                {this.state.isToogleOn ? this.state.label[0] : this.state.label[1]}
            </button>
        )
    }

}

class LoginControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isLoggedIn: false}
        //this.handleLoginClick = this.handleLoginClick.bind(this);
        //this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }

    handleLoginClick = () => {
        this.setState({
            isLoggedIn: true
        });
    }

    handleLogoutClick = () => {
        this.setState({
            isLoggedIn: false
        });
    }

    render(){
        const isLoggedIn = this.state.isLoggedIn;
        let button;
        if(isLoggedIn)
            button = <LogoutButton onClick={this.handleLogoutClick}/>;
        else button = <LoginButton onClick={this.handleLoginClick}/>;

        return (
            <div>
                <Greetng isLoggedIn={isLoggedIn}/>
                <div>User {isLoggedIn? "logged": "not logged"}</div>
                {button}
            </div>
        )
    }
}

function UserGreeting(props){
    return <h1>Welcome back!</h1>;
}

function GuestGreeting(props){
    return <h1>Please, login!</h1>
}

function Greetng(props){
    const isLoggedIn = props.isLoggedIn;
    if(isLoggedIn)
        return <UserGreeting/>;
    else return <GuestGreeting/>;
}

function LoginButton(props) {
    return <button onClick={props.onClick}>Войти</button>
}

function LogoutButton(props){
    return <button onClick={props.onClick}>Выйти</button>
}

function Mailbox(props){
    const unreadMessages = props.unreadMessages;
    return (
        <div>
            <h1>Hi!</h1>
            {unreadMessages.length > 0 &&
            <h2>
                Your {unreadMessages.length} not read messages
            </h2>}
        </div>
    )
}

const unreadMessages = ['React', 'Re: React', 'Re: Re: React'];

function WarningBanner(props){
    if(!props.warn){
        return null;
    }
    return (
        <div className="warning">
            Warning!
        </div>
    )
}

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showWarning: true};
    }
    handleToggleClick = () => {
        this.setState(state => ({
            showWarning: !state.showWarning
        }))
    }
    render() {
        return (
            <div>
                <WarningBanner warn={this.state.showWarning}/>
                <button onClick={this.handleToggleClick}>
                    {this.state.showWarning?"Hide":"Show"}
                </button>
            </div>
        )
    }
}

function Items(props) {
    //const numbers = [1,2,3,4,5];
    const numbers = props.numbers;
    const listItems = numbers.map((number) => <li key={number.id.toString()}>{number.number*2}</li>);
    return (
        <ul>{listItems}</ul>
    )
}

//const numbers = [1,2,3,4,5];
const numbers = [{id:1, number: 1},
                {id:2, number: 2},
                {id:3, number: 3},
                {id:4, number: 4},
                {id:5, number: 5}];

function App(){
    return (
        <div>
            <Clock />
            <Clock />
            <Clock />
            <Toogle label={['Yes', 'No']}/>
            <LoginControl/>
            <Mailbox unreadMessages={unreadMessages}/>
            <Page />
            <Items numbers={numbers}/>
        </div>
    )
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);