import React, {useReducer} from 'react'
import SimpleReactValidator from 'simple-react-validator'


export default function useSimpleReactValidator(passInOptions = {}) {
    const [{ options }, forceUpdate] = useReducer(({ options }) => ({ options }), {
        options: passInOptions,
    });
    SimpleReactValidator.addLocale('ru', {
        required: 'Поле :attribute обязательно к заполнению.',
    })
    const simpleValidator = React.useMemo(
        () =>
            new SimpleReactValidator(
                options.autoForceUpdate
                    ? {
                        ...options,
                        locale: 'ru',
                        className: 'text-danger',
                        autoForceUpdate: {
                            forceUpdate,
                        },
                    }
                    : options
            ),
        [options]
    );

    return [simpleValidator, forceUpdate];
}