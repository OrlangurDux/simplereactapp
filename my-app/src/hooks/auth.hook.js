import {useState, useCallback, useEffect} from 'react'

const storageName = 'userData'

export const useAuth = () => {
  const [token, setToken] = useState(null)
  const [ready, setReady] = useState(false)

  const login = useCallback((token) => {
    setToken(token)

    sessionStorage.setItem(storageName, JSON.stringify({
      token: token
    }))
  }, [])


  const logout = useCallback(() => {
    setToken(null)
      sessionStorage.removeItem(storageName)
  }, [])

  useEffect(() => {
    const data = JSON.parse(sessionStorage.getItem(storageName))

    if (data && data.token) {
      login(data.token)
    }
    setReady(true)
  }, [login])


  return { login, logout, token, ready }
}
