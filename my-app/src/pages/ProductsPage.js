import React, {useCallback, useEffect, useState, useContext} from 'react'
import {ProductList} from "../components/products/ProductList";
import {Loader} from "../components/Loader";
import {useHttp} from "../hooks/http.hook";
import ApiRoute from "../config/api";
import {AuthContext} from "../context/AuthContext";
import {Link} from 'react-router-dom'

export const ProductsPage = () => {

    const [products, setProducts] = useState([])
    const {loading, request} = useHttp()
    const {token} = useContext(AuthContext)

    const getProductsList = useCallback(async () => {
        try {
            const fetched = await request(ApiRoute.products.list, 'GET', null, {
                'Authorization': `${token}`
            })
            setProducts(fetched.data)
        } catch (e) {}
    }, [token, request])

    useEffect(() => {
        getProductsList()
    }, [getProductsList])

    return (
        <div className="container">
            <h2 className="mb-3">Продукция</h2>
            <Link to='/products/add' className="btn btn-success mb-4">Добавить продукцию</Link>
            <div className="">
                {loading ? <Loader /> : <ProductList products={products} getProductsList={getProductsList} />}
            </div>
        </div>
    )
}