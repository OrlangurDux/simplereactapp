import React, {useEffect, useState, useCallback} from 'react'
import {UsersList} from "../components/users/UsersList";
import {DisplayBoard} from "../components/DisplayBoard";
import {CreateUser} from "../components/users/CreateUser";
import {useHttp} from "../hooks/http.hook";
import ApiRoute from "../config/api"
import {Loader} from "../components/Loader";

export const UsersPage = () => {

    const [users, setUsers] = useState([])
    const [numberOfUsers, setNumberOfUsers] = useState(users.length)
    const {loading, request} = useHttp()

    const getUsersList = useCallback(async () => {
        try {
            const fetched = await request(ApiRoute.users.list, 'GET', null)
            setUsers(fetched.data)
        } catch (e) {}
    }, [request])

    useEffect(() => {
        getUsersList()
    }, [getUsersList])

    useEffect(() => {
        setNumberOfUsers(users.length)
    }, [users.length])

    return(
        <>
            <div className="row">
                <div className="col-md-8">
                    <CreateUser getUsersList={getUsersList}/>
                </div>
                <div className="col-md-4">
                    <DisplayBoard
                        numberOfUsers={numberOfUsers}
                        loading={loading}
                        getUsersList={getUsersList}>
                    </DisplayBoard>
                </div>
            </div>
            <div className="row mt-4">
                {loading ? <Loader /> : <UsersList users={users}/>}
            </div>
        </>


    )
}