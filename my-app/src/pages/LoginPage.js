import React, {useContext, useEffect, useState, useCallback} from 'react'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import ApiRoute from "../config/api";
import {Card, Button, Form, Spinner} from 'react-bootstrap'
import useSimpleReactValidator from "../hooks/validate.hook";

export const LoginPage = () => {
    const auth = useContext(AuthContext)
    const {loading, request, error, clearError} = useHttp()
    const validateRules = {};
    const [validator] = useSimpleReactValidator({...validateRules, autoForceUpdate: true});
    const [form, setForm] = useState({
        email: '', password: ''
    })

    useEffect(() => {
        clearError()
    }, [clearError])

    const changeHandler = event => {
        setForm({ ...form, [event.target.name]: event.target.value })
    }

    const login = useCallback(async () => {
        try {
            const response = await request(ApiRoute.login, 'POST', form)
            auth.login(response.data.token)
        } catch (e) {}
    }, [request, form, auth])

    const loginHandler = () => {
        if (validator.allValid()) {
            login()
        } else {
            validator.showMessages()
        }
    }


    return (
        <div className="row">
            <div className="col-4 offset-4">
                <Card>
                    <Card.Header>Авторизация</Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group controlId="formGroupEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control name="email" type="email" placeholder="Ваш email" onChange={changeHandler} onBlur={() => validator.showMessageFor('email')}/>
                                {validator.message('Email', form['email'], 'required|string')}
                            </Form.Group>
                            <Form.Group controlId="formGroupPassword">
                                <Form.Label>Пароль</Form.Label>
                                <Form.Control name="password" type="password" placeholder="Пароль" onChange={changeHandler} onBlur={() => validator.showMessageFor('password')}/>
                                {validator.message('Пароль', form['password'], 'required|string')}
                            </Form.Group>
                            <Button variant="primary" onClick={loginHandler} disabled={loading}>Войти {loading && <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true" />}</Button>
                            {error && <div className="alert alert-danger mt-2 small" role="alert">{error}</div>}
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}
