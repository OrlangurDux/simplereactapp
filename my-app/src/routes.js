import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {InformationPage} from './pages/InformationPage'
import {HomePage} from './pages/HomePage'
import {GraphPage} from './pages/GraphPage'
import {UsersPage} from "./pages/UsersPage";
import {LoginPage} from "./pages/LoginPage";
import {ProductsPage} from "./pages/ProductsPage";
import {ProductShow} from "./components/products/ProductShow";
import {ProductEdit} from "./components/products/ProductEdit";
import {ProductCreate} from "./components/products/ProductCreate";


const ProtectedRoute = ({ component: Component, ...props  }) => {
    return (
        <Route {...props}>
            {
                () => props.isAuthenticated ? <Component /> : <Redirect to="/" />
            }
        </Route>
    )}

const GuestRoute = ({ component: Component, ...props  }) => {
    return (
        <Route {...props}>
            {
                () => !props.isAuthenticated ? <Component /> : <Redirect to="/" />
            }
        </Route>
    )}

export const useRoutes = isAuthenticated => {

    return (
        <Switch>
            <Route exact path="/">
                <HomePage/>
            </Route>
            <GuestRoute
                exact path="/login"
                isAuthenticated={isAuthenticated}
                component={LoginPage}
            />
            <ProtectedRoute
                exact path="/products/add"
                isAuthenticated={isAuthenticated}
                component={ProductCreate}
            />
            <ProtectedRoute
                exact path="/products/:id/edit"
                isAuthenticated={isAuthenticated}
                component={ProductEdit}
            />
            <ProtectedRoute
                exact path="/products/:id"
                isAuthenticated={isAuthenticated}
                component={ProductShow}
            />
            <ProtectedRoute
                exact path="/products"
                isAuthenticated={isAuthenticated}
                component={ProductsPage}
            />
            <Route exact path="/users">
                <UsersPage/>
            </Route>
            <Route exact path="/info">
                <InformationPage label={"больше информации"}/>
            </Route>
            <Route exact path="/graph">
                <GraphPage/>
            </Route>
            <Redirect to="/"/>
        </Switch>
    )
}
